const initSettings = () => {
  const settingsDialog = document.getElementById('settings-dialog');

  document.getElementById('settings-icon').onclick = () => {
    openDialog(settingsDialog, onCloseSettings);
  };

  document.getElementById('setting-hide-date').checked = settingDateHidden();
  document.getElementById('setting-hide-shadow').checked = settingShadowHidden();
  document.getElementById('font-color').value = settingFontColor();
  document.getElementById('setting-search-engine').value = settingSearchEngine();
  applySettings();
};

const onCloseSettings = () => {
  localStorage.setItem('hideDate', document.getElementById('setting-hide-date').checked);
  localStorage.setItem('hideShadow', document.getElementById('setting-hide-shadow').checked);
  localStorage.setItem('fontColor', document.getElementById('font-color').value);
  localStorage.setItem('searchEngine', document.getElementById('setting-search-engine').value);
  applySettings();
};

const applySettings = () => {
  document.getElementById('date').style.visibility = settingDateHidden() ? 'hidden' : 'visible';

  const mainElement = document.getElementById('main');

  if (settingShadowHidden()) mainElement.classList.remove('text-shadow');
  else mainElement.classList.add('text-shadow');

  mainElement.style.color = settingFontColor();

  settingShadowHidden();
};

const settingDateHidden = () => {
  const value = localStorage.getItem('hideDate');
  return value === 'true';
};

const settingShadowHidden = () => {
  const value = localStorage.getItem('hideShadow');
  return value === 'true';
};

const settingFontColor = () => {
  const value = localStorage.getItem('fontColor');
  return value ? value : '#ffffff';
};

const settingSearchEngine = () => {
  const value = localStorage.getItem('searchEngine');
  return value ? value : 'duckduckgo';
};

const settingSearchEnginePrefix = () => {
  switch (settingSearchEngine()) {
    case 'duckduckgo':
    default:
      return 'https://duckduckgo.com/?q=';
    case 'ecosia':
      return 'https://www.ecosia.org/search?q=';
    case 'google':
      return 'https://www.google.com/search?q=';
    case 'qwant':
      return 'https://lite.qwant.com/?q=';
  }
};
