let activeDialog = null;

const dialogOnKeydown = (e) => {
  if (e.key === 'Escape' || e.key === 'Esc') {
    closeDialog();
  }
};

const closeDialog = () => {
  if (activeDialog) {
    activeDialog.style.display = 'none';

    activeDialog.onClose();
    delete activeDialog.onClose;

    activeDialog = null;
  }
};

const openDialog = (element, onClose) => {
  if (isDialog(element) && !activeDialog) {
    element.onclick = (e) => {
      if (e.target === element) closeDialog(element);
    };

    element.style.display = 'block';
    element.onClose = onClose;
    activeDialog = element;
  }
};

const isDialog = (element) => {
  return element && element.classList.contains('dialog');
};
