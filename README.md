# Rooty

This is the repository of the browser extension Rooty.
Rooty adds a beautiful, simple and effective home page to your browser.

# Functionality

Rooty tries to keep things simple. Opening a new tab will display a nice background image as well as the current time and date.
When you input any text on the page your bookmarks will get searched and displayed.
Press enter to open the topmost bookmark or perform a web search.

## Example screenshot
![screenshot](docs/example.png "New tab")

# License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Background images

I do not own any of the images displayed as background images.
All those images are obtained via the website https://pixabay.com and are free of copyright.